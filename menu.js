//Get the elements necessary
let searchButton = document.getElementById("searchButton");
let searchText = document.getElementById("searchText");
let bookmarksList = document.getElementById("bookmarks");
let folderSelect = document.getElementById("folderSelect");
let sortButton = document.getElementById("sortButton");

//When the add-on is started,
//It searches all the bookmark
findBookmarks();
//And all the folders inside the bookmarks
findFolders()


//Events

//When we click on the button "Search", it search for the bookmark
searchButton.addEventListener('click', event => {
    findBookmarks();
});

//When we press the "Enter" key, it search for the bookmark
searchText.addEventListener('keydown', event => {
    if(event.keyCode == 13) {
        findBookmarks();
    }
});

//When we click on the button "Sort", it search for the bookmark
sortButton.addEventListener('click', event => {
    getFolders(folderSelect.value);
});


//Functions

//Change what's inside each li element
//If the class is updated, then the bookmark can be changed
//If not, it only shows the title and a wrench
//It clear what's inside the element and replace it
function toggleUpdateBookmarks(element, bookmark) {
    if(element.className == "update") {
        let div1 = document.createElement('div');

        let bookmarkTitle = document.createElement('input');
        bookmarkTitle.type = "text";
        bookmarkTitle.value = bookmark.title;

        let bookmarkUrl = document.createElement('input');
        bookmarkUrl.type = "text";
        bookmarkUrl.value = bookmark.url;

        let div2 = document.createElement('div');
        let saveButton = document.createElement('input');
        saveButton.type = "button";
        saveButton.value = "Save";
        saveButton.id = "saveButton";
        saveButton.className = "updateButtons";
        saveButton.addEventListener("click", event => {
            let updating = browser.bookmarks.update(bookmark.id, {
                title: bookmarkTitle.value,
                url: bookmarkUrl.value
            });
            updating.then(data => {
                element.className = "bookmark";
                toggleUpdateBookmarks(element, data);
            });
        });

        let cancelButton = document.createElement('input');
        cancelButton.type = "button";
        cancelButton.value = "Cancel";
        cancelButton.id = "cancelButton";
        cancelButton.className = "updateButtons";
        cancelButton.addEventListener("click", event => {
            element.className = "bookmark";
            toggleUpdateBookmarks(element, bookmark);
        });

        let deleteButton = document.createElement('input');
        deleteButton.type = "button";
        deleteButton.value = "Delete";
        deleteButton.id = "deleteButton";
        deleteButton.className = "updateButtons";
        deleteButton.addEventListener("click", event => {
            element.className = "bookmark";
            browser.bookmarks.remove(bookmark.id);
            element.remove();
        });
        
        element.innerHTML = '';
        div1.appendChild(bookmarkTitle);
        div1.appendChild(bookmarkUrl);
        element.appendChild(div1);
        div2.appendChild(saveButton);
        div2.appendChild(cancelButton);
        div2.appendChild(deleteButton);
        element.appendChild(div2);

    } else {
        element.innerHTML = '';

        let div = document.createElement('div');
        div.innerHTML = bookmark.title;
        element.appendChild(div);

        let updateButton = document.createElement('input');
        updateButton.type = "button";
        updateButton.value = "🔧";
        updateButton.className = "wrench";
        element.appendChild(updateButton);

        updateButton.addEventListener("click", event => {
            element.className = "update";
            toggleUpdateBookmarks(element, bookmark);
        });
    }
}

//Find a bookmark where the searchText.value
// is inside the title, url of the bookmark
function findBookmarks() {
    //Remove the child of the bookmarksList
    bookmarksList.innerHTML = '';

    //Search for result in bookmarks
    let searching = browser.bookmarks.search({query: searchText.value});
    searching.then((bookmarks) => {
        //Add the result to the bookmarksList
        let i = 0;
        for(; i < bookmarks.length; i++) {
            let bookmark = bookmarks[i];
            if(bookmark.type == "bookmark") {
                let li = document.createElement('li');
                li.className = "bookmark";
                toggleUpdateBookmarks(li, bookmark);
                //If the user double click on the bookmark
                // and he is not updating it,
                // it opens in a new tab
                li.addEventListener("dblclick", event => {
                    if(li.className != "update") {
                        window.open(bookmark.url, "_blank");
                    }
                });
                bookmarksList.appendChild(li);
            }
        }
    });
}

//Find all the folders inside the bookmarks
function findFolders() {
    //Remove the child of the folderSelect
    folderSelect.innerHTML = '';

    //Search for result in bookmarks
    let searching = browser.bookmarks.search({query: ""});
    searching.then((bookmarks) => {
        //Add the result to the folderSelect
        let i = 0;
        for(; i < bookmarks.length; i++) {
            if(bookmarks[i].type == "folder") {
                let option = document.createElement('option');
                option.innerHTML = bookmarks[i].title;
                option.value = bookmarks[i].id;
                folderSelect.appendChild(option);
            }
        }
    });
}

//Check if there's folder inside the folder to order
function getFolders(parentFolderId) {
    let folders = [];

    let searching = browser.bookmarks.search({query: ""});
    searching.then((bookmarks) => {
        let i = 0;
        for(; i < bookmarks.length; i++) {
            if(bookmarks[i].type == "folder" && bookmarks[i].parentId == parentFolderId) {
                //If yes, it register it inside "folders"
                folders.push([bookmarks[i].title, bookmarks[i]]);
            }
        }
    });
    sortFolder(folders, parentFolderId);
}

//Sort the selected folder (folder with an ID)
function sortFolder(folders, parentFolderId) {
    let searching = browser.bookmarks.search({query: ""});
    searching.then( async (bookmarks) => {
        
        let i = 0;
        for(; i < bookmarks.length; i++) {
            let bookmark = bookmarks[i];
            if(bookmarks[i].type != "folder" && bookmark.parentId == parentFolderId) {
                //Get the name of the folder (ex: www.mozilla.org)
                const regex = /\/\/[A-Za-z0-9.]*/g;
                let found = bookmark.url.match(regex);
                let folderName = found[0].substring(2);

                let j = 0;
                let folder;
                for(; j < folders.length; j++) {
                    if(folders[j][0] == folderName) {
                        folder = folders[j][1];
                    }
                }
                if(folder == null) {
                    folder = await browser.bookmarks.create({type: "folder", parentId: parentFolderId, title: folderName});
                    folders.push([folder.title, folder]);
                }
                browser.bookmarks.move(bookmark.id, {parentId: folder.id});
            }
        }
    });
    //Show that it's done
    folderSelect.options[folderSelect.selectedIndex].text += ' ✅';
}