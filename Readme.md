# Bookmarks-modifier

## JS + HTML + CSS
This is an add-on for Mozzila firefox.

This add-on allows the user to:
- Find a bookmark by title or url.
- Open the bookmark (when double-click on it).
- Change more quickly the title or the url.
- Delete a bookmark.
- Sort a folder by url in a folder with the website name (look the images).

It requires only the bookmarks permission.

To develop it, I have used :
- To run the add-on (development): web-ext run
- To create the build: web-ext build --overwrite-dest
- Use the console: Ctrl + Shift + J

You can try it on :
https://addons.mozilla.org/fr/firefox/addon/bookmarks-modifier/